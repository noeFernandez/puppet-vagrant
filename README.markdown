# Vagrant puppet module #

This is the vagrant module. It provides a vagrant installation for windows 

You can user this module typing 

```puppet
class{'vagrant':
    ensure => installed,    #by default this value is set to installed
    version => 1.7.1,
}
```


