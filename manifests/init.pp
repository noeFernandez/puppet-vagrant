# Class: vagrant
class vagrant ($ensure = installed, $version = $::vagrant::params::version) inherits vagrant::params {
  $localpath = "C:\\Windows\\Temp\\vagrant_${version}.msi"

  case $::operatingsystem {
    windows : {
      download { "Download Vagrant ${version}":
        source      => "https://dl.bintray.com/mitchellh/vagrant/vagrant_${version}.msi",
        destination => $localpath,
      } ->
      package { "Vagrant":
        ensure          => $ensure,
        source          => $localpath,
        install_options => ["/quiet"],
      }
    }
  }

}

